<?php

namespace App\Listeners;

use App\Enumerations\TransactionEnum;
use App\Enumerations\TransactionTypeEnum;
use App\Models\Payment;
use App\Models\PreOrder;
use App\Models\Transaction;
use App\Models\User;
use App\Services\Financial\Financial;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Shetabit\Multipay\Invoice;
use Shetabit\Multipay\Receipt;

class VerifiedPaymentListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle($event)
    {

        try {
            /** @var Invoice $invoice */
            $invoice = $event->invoice;

            /** @var Receipt $receipt */
            $receipt = $event->receipt;

            $payment = Payment::where(Payment::AUTHORITY_ID, $invoice->getTransactionId())
                ->with(['user'])
                ->firstOrFail();

            $payment->update([
                Payment::REFERENCE_ID => $receipt->getReferenceId(),
                Payment::VERIFY_AT    => now(),
            ]);

            Transaction::create([
                Transaction::USER_ID     => $payment->{Payment::USER_ID},
                Transaction::PAYMENT_ID  => $payment->{Payment::ID},
                Transaction::TYPE_ENUM   => TransactionTypeEnum::INCREASE_CREDIT,
                Transaction::AMOUNT      => $payment->{Payment::AMOUNT},
                Transaction::DESCRIPTION => $this->getMessage(TransactionTypeEnum::INCREASE_CREDIT, $payment->{Payment::ORDER_ID} ?? $payment->{Payment::REFERENCE_ID}),
            ]);

            Transaction::create([
                Transaction::USER_ID     => $payment->{Payment::USER_ID},
                Transaction::PAYMENT_ID  => $payment->{Payment::ID},
                Transaction::TYPE_ENUM   => TransactionTypeEnum::BUY_PLAN,
                Transaction::AMOUNT      => $payment->{Payment::AMOUNT} * -1,
                Transaction::DESCRIPTION => $this->getMessage(TransactionTypeEnum::BUY_PLAN, $payment->{Payment::ORDER_ID} ?? $payment->{Payment::REFERENCE_ID}),
            ]);

            $user = User::find($payment->{Payment::USER_ID});
            $user->{User::PLAN_EXPIRED_AT} = now()->addMonth();
        } catch (\Exception $exception) {

            report($exception);
        }
    }

    public function getMessage($typeEnum, $orderId = null): string
    {
        return trans('templates.transaction.' . $typeEnum, ['id' => $orderId]);
    }

}
