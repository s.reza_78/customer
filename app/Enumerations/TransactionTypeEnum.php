<?php

namespace App\Enumerations;

class TransactionTypeEnum
{
    const INCREASE_CREDIT = 1;
    const BUY_PLAN = 2;
}
