<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Role;

class AddSuperAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add super admin';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $userId = $this->ask('add user id :');
        $user = User::find($userId);

        $user->assignRole('Super Admin');

        $this->info("hi reza;new super admin created :)))))))))");
        return Command::SUCCESS;
    }
}
