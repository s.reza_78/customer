<?php

namespace App\Http\Controllers;


use App\Enumerations\TransactionTypeEnum;
use App\Models\Payment;
use App\Services\payment\PaymentService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Shetabit\Payment\Facade\Payment as PaymentGate;

class PaymentController extends Controller
{
    public function pay(Request $request)
    {

        return PaymentService::payUrl(Auth::user(), 4900000, TransactionTypeEnum::INCREASE_CREDIT);
    }

    public function verifyPayment(Request $request)
    {

        try {

            $authorityId = $request->Authority;

            if (!$authorityId) {

                alert()->error('خطا در پرداخت');
                return redirect('/dashboard');
            }

            $payment = Payment::where(Payment::AUTHORITY_ID, $authorityId)->first();

            if (!$payment) {

                alert()->error('خطا در پرداخت');
                return redirect('/transactions');
            }

            PaymentGate::amount($payment->{Payment::AMOUNT} / 10)
                ->via('zarinpal')
                ->transactionId($authorityId)
                ->verify();


            alert()->success('پرداخت موفق');
            return redirect('/transactions');
        } catch (\Exception $exception) {

            report($exception);

            alert()->error('خطا در پرداخت');
            return redirect('/transactions');
        }
    }
}
