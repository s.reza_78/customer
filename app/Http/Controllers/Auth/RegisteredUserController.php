<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Shop;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'     => ['required', 'string', 'max:255'],
            'mobile'   => ['required', 'string', 'min:11', 'max:11', 'unique:users,mobile'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ],
            [
                'name.required'      => "فیلد نام اجباری می باشد",
                'shop_id.required'   => "فیلد فروشگاه اجباری می باشد",
                'email.required'     => "فیلد ایمیل اجباری می باشد",
                'password.required'  => "فیلد رمز عبور اجباری می باشد",
                'email.unique'       => "این ایمیل قبلا  ثبت شده است ",
                "password.confirmed" => "تکرار رمز عبور با رمز عبور مغایرت دارد."
            ]);

        $user = User::create([
            'name'                => $request->name,
            'mobile'              => $request->mobile,
            'password'            => Hash::make($request->password),
            User::PLAN_EXPIRED_AT => now()->addDays(7)
        ]);

        event(new Registered($user));

        Auth::login($user);

        return redirect('/dashboard');
    }
}
