<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCustomerRequest;
use App\Models\Customer;
use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomerController
{

    public function index(Request $request)
    {
        $customers = Customer::where(Customer::USER_ID, Auth::id())
            ->orderBy('id', 'DESC')
            ->paginate(15);


        return view('customers.list', compact('customers'));
    }


    public function store(CreateCustomerRequest $request)
    {
        try {
            $customer = Customer::where(Customer::MOBILE, $request->post('mobile'))->first();
            $data = $request->all();
            $data['shop_id'] = $request->post('shop_id', Auth::user()->shops()->first()->id);
            $data['user_id'] = Auth::id();

            if (!$customer) {
                $customer = new Customer();
                foreach ($data as $column => $value) {
                    if (in_array($column, $customer->getFillable())) {
                        $customer->{$column} = $value;
                    }
                }

                $customer->save();
            }

            if ($request->post('shop_id')) {
                $shop = Shop::find($request->post('shop_id'));
                $shop->customers()->save($customer);
            }
        } catch (\Exception $e) {
            alert()->error('خطا در ساخت مشتری به پشتیبانی اطلاع دهید');
        }


        alert()->success('مشتری با موفقیت اضافه شد.');

        return redirect()->back();
    }

    public function create()
    {
        $shops = Shop::where(Shop::USER_ID, Auth::id())->get();

        return view('customers.add', compact('shops'));
    }

    public function sendMessage(Request $request)
    {
//        $text =
    }
}
