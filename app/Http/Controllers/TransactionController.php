<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    public function list(Request $request)
    {
        $transactions = Transaction::orderBy('id', 'DESC')
            ->where(Transaction::USER_ID, Auth::id())
            ->paginate(15);

        return view('transactions.list', compact('transactions'));
    }
}
