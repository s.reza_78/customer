<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shop extends Model
{
    use HasFactory,SoftDeletes;

    const TITLE = 'title';
    const USER_ID = 'user_id';

    protected $fillable = [
        self::TITLE,
        self::USER_ID
    ];

    public function customers()
    {
        return $this->belongsToMany(Customer::class, 'shop_customers');
    }

    public function user()
    {
        return $this->hasOne(User::class);
    }
}
