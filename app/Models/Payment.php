<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Payment extends Model
{
    use HasFactory, SoftDeletes;

    const ID = 'id';
    const USER_ID = 'user_id';
    const ORDER_ID = 'order_id';
    const AMOUNT = 'amount';
    const TYPE_ENUM = 'type_enum';
    const GATEWAY_ENUM = 'gateway_enum';
    const AUTHORITY_ID = 'authority_id';
    const REFERENCE_ID = 'reference_id';
    const VERIFY_AT = 'verified_at';
    const DELETED_AT = 'deleted_at';

    /**
     * @var string[]
     */
    protected $casts = [
        self::USER_ID      => 'int',
        self::ORDER_ID     => 'int',
        self::AMOUNT       => 'int',
        self::GATEWAY_ENUM => 'int',
        self::TYPE_ENUM    => 'int',
        self::AUTHORITY_ID => 'string',
        self::REFERENCE_ID => 'string'
    ];

    /**
     * @var array
     */
    protected $dates = [
        self::CREATED_AT,
        self::UPDATED_AT,
        self::DELETED_AT,
        self::VERIFY_AT,
    ];

    /**
     * @var array
     */
    protected $fillable = [
        self::ID,
        self::USER_ID,
        self::ORDER_ID,
        self::AMOUNT,
        self::GATEWAY_ENUM,
        self::TYPE_ENUM,
        self::AUTHORITY_ID,
        self::REFERENCE_ID,
        self::VERIFY_AT,
        self::DELETED_AT,
    ];

    public function user()
    {

        return $this->hasOne(User::class, User::ID, self::USER_ID);
    }

}
