<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    const ID = 'id';
    const USER_ID = 'user_id';
    const NAME = 'name';
    const MOBILE = 'mobile';
    const GENDER = 'gender';
    const BIRTHDATE = 'birthdate';

    protected $fillable = [
        self::USER_ID,
        self::NAME,
        self::MOBILE,
        self::GENDER,
        self::BIRTHDATE
    ];

    public function shops()
    {
        return $this->belongsToMany(Shop::class, 'shop_customers');
    }

//    public function scopeSearch($query, $request)
//    {
//        Customer::where(Customer::MOBILE,$request->get('search'));
//
//    }
}
