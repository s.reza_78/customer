<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles, SoftDeletes;

    const ID = 'id';
    const NAME = 'name';
    const EMAIL = 'email';
    const MOBILE = 'mobile';
    const PASSWORD = 'password';
    const PLAN_EXPIRED_AT = 'plan_expired_at';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        self::MOBILE,
        self::PLAN_EXPIRED_AT
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at'   => 'datetime',
        self::PLAN_EXPIRED_AT => 'datetime',
    ];

    public function shops()
    {
        return $this->hasMany(Shop::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}
