<?php

namespace App\Models;

use App\Enumerations\TransactionEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Transaction extends Model
{
    use HasFactory, SoftDeletes;

    const ID = 'id';
    const USER_ID = 'user_id';
    const SEND_ADS_ID = 'send_ads_id';
    const PAYMENT_ID = 'payment_id';
    const TYPE_ENUM = 'type_enum';
    const AMOUNT = 'amount';
    const DESCRIPTION = 'description';
    const DELETED_AT = 'deleted_at';
    const CREATED_AT = 'created_at';

    /**
     * @var string[]
     */
    protected $casts = [
        self::USER_ID    => 'int',
        self::PAYMENT_ID => 'string',
        self::TYPE_ENUM  => 'int',
        self::AMOUNT     => 'int',
    ];

    /**
     * @var array
     */
    protected $dates = [
        self::CREATED_AT,
        self::UPDATED_AT,
        self::DELETED_AT,
    ];

    /**
     * @var array
     */
    protected $fillable = [
        self::ID,
        self::USER_ID,
        self::PAYMENT_ID,
        self::SEND_ADS_ID,
        self::TYPE_ENUM,
        self::AMOUNT,
        self::DESCRIPTION,
    ];

    public function advertising()
    {
        return $this->belongsTo(SendAdvertising::class);
    }
}
