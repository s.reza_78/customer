<?php


namespace App\Services;

use Illuminate\Support\Facades\Http;
use SoapClient;

class SmsService
{
    protected string $baseUrl = 'http://sms.wecan-co.ir/wbs';
    protected array $config;

    public function __construct()
    {
        ini_set("soap.wsdl_cache_enabled", "0");

        $this->config = config('sms.wecan');
    }

    public function otp(array $mobiles, int $code)
    {
        try {
            $soap = new SoapClient($this->baseUrl . "/send.php?wsdl");
            $soap->token = $this->config['token'];
            $soap->fromNum = $this->config['service_number'];
            $soap->toNum = $mobiles;
            $soap->patternID = $this->config['otp_pattern_id'];
            $soap->Content = json_encode([
                'otp' => $code
            ], JSON_UNESCAPED_UNICODE);
            $soap->Type = 0;

            return $soap->SendSMSByPattern($soap->fromNum, $soap->toNum, $soap->Content, $soap->patternID, $soap->Type, $soap->token);
        } catch (\Exception $ex) {
            report($ex);
        }

        return null;
    }

    public function send(array $mobiles, string $message)
    {
        try {
            $soap = new SoapClient($this->baseUrl . "/send.php?wsdl");
            $soap->token = $this->config['token'];
            $soap->Username = $this->config['token'];
            $soap->Password = $this->config['password'];
            $soap->fromNum = $this->config['special_number'];
            $soap->toNum = $mobiles;
            $soap->Content = $message;
            $soap->Type = 0;

            return $soap->SendSMS($soap->fromNum, $soap->toNum, $soap->Content, $soap->Type, $soap->Username, $soap->Password);
        } catch (\Exception $ex) {
            dd($ex->getMessage());
            report($ex);
        }

        return null;
    }

    public function service(array $mobiles, string $message)
    {
        try {
            $soap = new SoapClient($this->baseUrl . "/send.php?wsdl");
            $soap->token = $this->config['token'];
            $soap->Username = $this->config['token'];
            $soap->Password = $this->config['password'];
            $soap->fromNum = $this->config['service_number'];
            $soap->toNum = $mobiles;
            $soap->Content = $message;
            $soap->Type = 0;

            return $soap->SendSMS($soap->fromNum, $soap->toNum, $soap->Content, $soap->Type, $soap->Username, $soap->Password);
        } catch (\Exception $ex) {
            report($ex);
        }

        return null;
    }

    public function status($code)
    {

        try {
            $response = Http::withHeaders([
                'token' => $this->config['token']
            ])
                ->post('http://185.112.33.62/api/v1/rest/sms/status',['idArray'=>[$code]])
                ->json();

        } catch (\Exception $ex) {
            report($ex);
        }

        return  $response['result']['result'][0]['state']?? null;
    }
}
