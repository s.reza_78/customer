<?php

namespace App\Services\payment;

use App\Models\Payment;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use Shetabit\Multipay\Invoice;

class PaymentService
{
    /**
     * @throws \Exception
     */
    public static function payUrl($user, int $amount, $type = null)
    {

//        $driverName = request()->get('pg', config('payment.default'));

        $driver = 'idpay';
        $invoice = (new Invoice)->amount($amount / 10)
            ->detail([
                'description' => "شارژ کیف پول",
                'mobile'      => $user->{User::MOBILE} ?? null
            ]);

        $config = config('payment');


        $payment = new \Shetabit\Multipay\Payment($config);

        $payment->purchase($invoice, function ($driver, $transactionId) use ($invoice, $amount, $type, $user) {
            Payment::create([
                Payment::USER_ID      => $user->{User::ID} ?? null,
                Payment::AMOUNT       => $amount,
                Payment::AUTHORITY_ID => $transactionId,
            ]);
        });

        return $payment->pay()->render();
    }
}
