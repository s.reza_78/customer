<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\TransactionController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dashboard', function () {
    return view('layouts.dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::get('/', function () {
    return redirect('/dashboard');
})->middleware(['auth', 'verified']);

Route::get('login', [AuthenticatedSessionController::class, 'show'])->name('login');
Route::post('login', [AuthenticatedSessionController::class, 'store'])->name('login');

Route::get('register', [RegisteredUserController::class, 'create'])->name('register');

Route::post('register', [RegisteredUserController::class, 'store']);

Route::get('/home', function () {
    redirect('/dashboard');
})->name('home')->middleware(['auth']);;
Route::get('/logout', function () {
    Auth::logout();
})->name('logout');

Route::get('/transactions', [TransactionController::class, 'list'])->name('web.user.transaction.list')->middleware(['auth']);

Route::resource('shops', \App\Http\Controllers\ShopController::class)->middleware('auth');
Route::group(['prefix' => 'customers'], function () {
    Route::get('/', [CustomerController::class, 'index'])->name('customers.index')->middleware(['auth']);;
    Route::post('/', [CustomerController::class, 'store'])->name('customers.store')->middleware(['auth']);;
    Route::get('/create', [CustomerController::class, 'create'])->name('customers.create')->middleware(['auth']);;
});

Route::post('payment', [PaymentController::class, 'pay'])->middleware(['auth'])->name('web.payment');
Route::view('payment', 'payment.payment-show')->middleware(['auth'])->name('web.payment');
Route::get('/payment/verify', [PaymentController::class, 'verifyPayment'])->name('payment.verify');
