{{--@can('super-admin')--}}

    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign Up Form by Colorlib</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="{{asset('fonts/material-icon/css/material-design-iconic-font.min.css')}}">

    <!-- Main css -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <!-- Font Awesome -->
    <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css"
        rel="stylesheet"
    />
    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
        rel="stylesheet"
    />
    <!-- MDB -->
    <link
        href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/5.0.0/mdb.min.css"
        rel="stylesheet"
    />
</head>
<body>

<div class="main">
    @if ($errors->any())
        <div class="alert alert-danger" dir="rtl">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <!-- Sign up form -->
    <section class="signup">
        <div class="container">
            <div class="signup-content">
                <div class="signup-form">
                    <h2 class="form-title text-end">ثبت نام</h2>
                    <form method="POST" class="register-form" id="register-form" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group">
                            <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                            <input type="text" name="name" id="name" placeholder="نام"
                            />
                        </div>
                        <div class="form-group">
                            <label for="mobile"><i class="zmdi zmdi-phone"></i></label>
                            <input type="number" name="mobile" id="mobile" placeholder="موبایل"
                            />

                        </div>
                        <div class="form-group">
                            <label for="pass"><i class="zmdi zmdi-lock"></i></label>
                            <input type="password" name="password" id="pass"
                                   placeholder=" رمز عبور"/>

                        </div>
                        <div class="form-group">
                            <label for="re-pass"><i class="zmdi zmdi-lock-outline"></i></label>
                            <input type="password" name="password_confirmation" id="re_pass"
                                   placeholder="تکرار رمز عبور"/>
                        </div>

{{--                        <div class="form-group select">--}}
{{--                            <select class="form-control select2" name="shop_id" style="width: 100%;">--}}
{{--                                <option>فروشگاه را انتخاب کنید</option>--}}
{{--                                @foreach($shops as $shop)--}}
{{--                                    <option value="{{$shop->id}}">{{$shop->title}}</option>--}}
{{--                                @endforeach--}}
{{--                            </select>--}}
{{--                        </div>--}}
                        {{--                        <div class="form-group">--}}
                        {{--                            <input type="checkbox" name="agree-term" id="agree-term" class="agree-term"/>--}}
                        {{--                            <label for="agree-term" class="label-agree-term"><span><span></span></span>I agree all--}}
                        {{--                                statements in <a href="#" class="term-service">Terms of service</a></label>--}}
                        {{--                        </div>--}}
                        <div class="form-group form-button">
                            <input type="submit" name="signup" id="signup" class="form-submit fw-bold" value="ثبت نام"/>
                        </div>
                    </form>

                </div>
                <div class="signup-image">
                    <figure><img src="{{asset('images/signup-image.jpg')}}" alt="sing up image"></figure>
                    <a href="{{route('login')}}" class="signup-image-link fw-bold">ورود</a>
                </div>

            </div>
        </div>
    </section>

</div>

<!-- JS -->
<script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>
{{--@endcan--}}
