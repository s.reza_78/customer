@extends('layouts.dashboard')

@section('content')
    <div class="card-header">
        <h3 class="card-title">لیست تراکنش های مالی</h3>
    </div>
    <!-- /.card-header -->
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>#</th>

            <th>مبلغ</th>
            <th>توضیحات</th>
            {{--            <th>تاریخ</th>--}}
        </tr>
        </thead>
        <tbody>
        @foreach($transactions as $transaction)
            <tr>

                <th>{{$loop->iteration}}</th>
                <th class="{{$transaction->{\App\Models\Transaction::AMOUNT} < 0 ? 'text-danger' :'text-success'}}">{{number_format(abs($transaction->{\App\Models\Transaction::AMOUNT})) }}
                    ریال
                </th>
                <th>{{$transaction->{\App\Models\Transaction::DESCRIPTION} }} </th>
                {{--                <th>{{$transaction->{\App\Models\Transaction::CREATED_AT} }} </th>--}}
            </tr>
        @endforeach

        </tbody>

    </table>

    {!! $transactions->links() !!}

    <!-- /.card-body -->

@endsection
<script src="{{asset('plugins/datatables/dataTables.bootstrap4.js')}}"></script>
<script>
    $(function () {
        $("#example1").DataTable({
            "language": {
                "paginate": {
                    "next": "بعدی",
                    "previous": "قبلی"
                }
            },
            "info": false,
        });
        $('#example2').DataTable({
            "language": {
                "paginate": {
                    "next": "بعدی",
                    "previous": "قبلی"
                }
            },
            "info": false,
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "autoWidth": false
        });
    });
</script>
