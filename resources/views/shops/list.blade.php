@extends('layouts.dashboard')

@section('content')
    <div class="card-header">
        <h3 class="card-title">لیست مشتریان</h3>
    </div>
    <!-- /.card-header -->
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>نام</th>

            {{--            <th>تولد</th>--}}
        </tr>
        </thead>
        <tbody>
        @foreach($shops as $shop)

            <tr>
                <th>{{$loop->iteration}}</th>
                <th>{{$shop->{\App\Models\Shop::TITLE}  }}</th>
                {{--                <th>{{$customer->{\App\Models\Customer::BIRTHDATE} ?? "خالی" }}</th>--}}
            </tr>
        @endforeach

        </tbody>
    </table>
    <!-- /.card-body -->

@endsection
<script src="{{asset('plugins/datatables/dataTables.bootstrap4.js')}}"></script>
<script>
    $(function () {
        $("#example1").DataTable({
            "language": {
                "paginate": {
                    "next": "بعدی",
                    "previous": "قبلی"
                }
            },
            "info": false,
        });
        $('#example2').DataTable({
            "language": {
                "paginate": {
                    "next": "بعدی",
                    "previous": "قبلی"
                }
            },
            "info": false,
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "autoWidth": false
        });
    });
</script>
