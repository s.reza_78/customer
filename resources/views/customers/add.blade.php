@extends('layouts.dashboard')



@section('content')
    @if ($errors->any())
        <div class="alert alert-danger" dir="rtl">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">ثبت مشتریان</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" action="{{route('customers.store')}}" method="POST">
                        @csrf

{{--                        <input class="form-control" name="shop_id"--}}
{{--                               type="hidden" >--}}
                        <div class="form-group">
                            <label>نام مشتری</label>
                            <input class="form-control" name="name"
                                   placeholder="نام">
                        </div>

                        <div class="form-group">
                            <label>شماره موبایل</label>
                            <input type="number" class="form-control" name="mobile"
                                   placeholder="شماره موبایل">
                        </div>

                        {{--                        <div class="form-group">--}}
                        {{--                            <label>فروشگاه</label>--}}
                        {{--                            <select class="form-control select2" name="shop_id" style="width: 100%;">--}}
                        {{--                                <option></option>--}}
                        {{--                                @foreach($shops as $shop)--}}
                        {{--                                    <option value="{{$shop->id}}">{{$shop->title}}</option>--}}
                        {{--                                @endforeach--}}
                        {{--                            </select>--}}
                        {{--                        </div>--}}
                        <div class="form-group">
                            <label>جنسیت</label>
                            <select class="form-control select2" name="gender" style="width: 100%;">
                                <option>انتخاب کنید.</option>
                                <option value="1">زن</option>
                                <option value="2">مرد</option>
                            </select>
                        </div>

                        <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">ثبت</button>
                                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.example1').pDatepicker();
        });
    </script>
@endsection

