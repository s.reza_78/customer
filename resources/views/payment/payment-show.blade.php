@extends('layouts.dashboard')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger" dir="rtl">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">خرید اشتراک ماهانه </h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->

                    <form role="form" action="{{route('web.payment')}}" method="POST">
                        @csrf

                        <div class="card-body">
                            <div class="form-group">
                                <div class="form-group">
                                    <h3>خرید اشتراک یک ماهه - <strike>100,000 تومان</strike> <br> با تخفیف ویژه 49,000 هزار تومان </h3>
                                </div>
                            </div>


                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" onclick="combine_data()" class="btn btn-primary">خرید</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


