<?php


use App\Enumerations\TransactionTypeEnum;

return [
    'transaction' => [

        TransactionTypeEnum::INCREASE_CREDIT => 'افزایش اعتبار به شماره پیگیری :id',
        TransactionTypeEnum::BUY_PLAN        => 'خرید اشتراک ماهانه',
    ],
];
