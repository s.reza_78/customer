<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Default Driver
    |--------------------------------------------------------------------------
    |
    | This value determines which of the following gateway to use.
    | You can switch to a different driver at runtime.
    |
    */
    'default'  => env('PAYMENT_DRIVER'),
    /*
    |--------------------------------------------------------------------------
    | List of Drivers
    |--------------------------------------------------------------------------
    |
    | These are the list of drivers to use for this package.
    | You can change the name. Then you'll have to change
    | it in the map array too.
    |
    */
    'drivers' => [
        'zarinpal'     => [
            /* normal api */
            'apiPurchaseUrl'              => 'https://api.zarinpal.com/pg/v4/payment/request.json',
            'apiPaymentUrl'               => 'https://www.zarinpal.com/pg/StartPay/',
            'apiVerificationUrl'          => 'https://api.zarinpal.com/pg/v4/payment/verify.json',

            /* sandbox api */
            'sandboxApiPurchaseUrl'       => 'https://sandbox.zarinpal.com/pg/services/WebGate/wsdl',
            'sandboxApiPaymentUrl'        => 'https://sandbox.zarinpal.com/pg/StartPay/',
            'sandboxApiVerificationUrl'   => 'https://sandbox.zarinpal.com/pg/services/WebGate/wsdl',

            /* zarinGate api */
            'zaringateApiPurchaseUrl'     => 'https://ir.zarinpal.com/pg/services/WebGate/wsdl',
            'zaringateApiPaymentUrl'      => 'https://www.zarinpal.com/pg/StartPay/:authority/ZarinGate',
            'zaringateApiVerificationUrl' => 'https://ir.zarinpal.com/pg/services/WebGate/wsdl',

            'mode'           => env('APP_ENV') === 'production' ? 'normal' : 'sandbox', // can be normal, sandbox, zaringate
            'merchantId'     => '54d06fe5-aed0-45e4-8c37-34955bef37d4', // New Amadast merchantId
            //            'merchantId' => '4dc3660c-0d77-4695-affe-0741719a2160', // Amadast merchantId
            //            'merchantId' => '6ef784d3-da2c-4d46-936f-9df007226356', // Karbalad merchantId
            'callbackUrl'    => 'http://localhost:8000/payment/verify',
            'currency' => 'T',
        ],
        'idpay' => [
            'apiPurchaseUrl' => 'https://www.zarinpal.com/pg/rest/WebGate/PaymentRequest.json',
            'apiPaymentUrl' => 'https://api.idpay.ir/v1.1/payment',
            'apiVerificationUrl' => 'https://api.idpay.ir/v1.1/payment/verify',
            'merchantId' => 'fcad44b1-dd1b-45b3-89b6-c40ca9e81370',
            'callbackUrl' => 'https://divar-bot.iran.liara.run/payment/verify',
//            'description' => 'payment in '.config('app.name'),
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Class Maps
    |--------------------------------------------------------------------------
    |
    | This is the array of Classes that maps to Drivers above.
    | You can create your own driver if you like and add the
    | config in the drivers array and the class to use for
    | here with the same name. You will have to extend
    | Shetabit\Multipay\Abstracts\Driver in your driver.
    |
    */
    'map'     => [
        'local'        => \Shetabit\Multipay\Drivers\Local\Local::class,
        'asanpardakht' => \Shetabit\Multipay\Drivers\Asanpardakht\Asanpardakht::class,
        'behpardakht'  => \Shetabit\Multipay\Drivers\Behpardakht\Behpardakht::class,
        'digipay'      => \Shetabit\Multipay\Drivers\Digipay\Digipay::class,
        'etebarino'    => \Shetabit\Multipay\Drivers\Etebarino\Etebarino::class,
        'idpay'        => \Shetabit\Multipay\Drivers\Idpay\Idpay::class,
        'irankish'     => \Shetabit\Multipay\Drivers\Irankish\Irankish::class,
        'nextpay'      => \Shetabit\Multipay\Drivers\Nextpay\Nextpay::class,
        'parsian'      => \Shetabit\Multipay\Drivers\Parsian\Parsian::class,
        'pasargad'     => \Shetabit\Multipay\Drivers\Pasargad\Pasargad::class,
        'payir'        => \Shetabit\Multipay\Drivers\Payir\Payir::class,
        'paypal'       => \Shetabit\Multipay\Drivers\Paypal\Paypal::class,
        'payping'      => \Shetabit\Multipay\Drivers\Payping\Payping::class,
        'paystar'      => \Shetabit\Multipay\Drivers\Paystar\Paystar::class,
        'poolam'       => \Shetabit\Multipay\Drivers\Poolam\Poolam::class,
        'sadad'        => \Shetabit\Multipay\Drivers\Sadad\Sadad::class,
        'saman'        => \Shetabit\Multipay\Drivers\Saman\Saman::class,
        'sepehr'       => \Shetabit\Multipay\Drivers\Sepehr\Sepehr::class,
        'walleta'      => \Shetabit\Multipay\Drivers\Walleta\Walleta::class,
        'yekpay'       => \Shetabit\Multipay\Drivers\Yekpay\Yekpay::class,
        'zarinpal'     => \Shetabit\Multipay\Drivers\Zarinpal\Zarinpal::class,
        'zibal'        => \Shetabit\Multipay\Drivers\Zibal\Zibal::class,
        'sepordeh'     => \Shetabit\Multipay\Drivers\Sepordeh\Sepordeh::class
    ]
];
